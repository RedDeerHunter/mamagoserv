package com.calgaryz.mamagoserv.model;

import java.util.List;

public class User {
    private String username;
    private String name;
    private String lastname;

    public User(String username, String name, String lastname){
        this.lastname = lastname;
        this.name = name;
        this.username = username;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setName(String name) { this.name = name; }

}