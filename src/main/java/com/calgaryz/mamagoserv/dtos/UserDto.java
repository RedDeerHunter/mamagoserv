package com.calgaryz.mamagoserv.dtos;

import com.calgaryz.mamagoserv.model.User;

public class UserDto {

    private long id;
    private String username;

    public UserDto(){
    }

    public static UserDto create(User user) {
        return new UserDto(user);
    }

    private UserDto(User user){
        this.username = user.getUsername();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}