package com.calgaryz.mamagoserv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MamagoservApplication {

	public static void main(String[] args) {
		SpringApplication.run(MamagoservApplication.class, args);
	}

}
